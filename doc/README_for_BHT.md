## Simulation of galaxy collision in 3D using the Barnes Hut Algorithm

---------
Summary of files and their functionalities:

In `jobs/src` directory:

1. "bht_algorithm_2D.py" or "bht_algorithm_3D.py":
-Adjusting Theta value.
-Adjusting the gravitational constant value.

In `tasks/src` directory:

2. "ff_simulation_3D.py":
- Adjusting initial conditions (positions, velocities).
- Choosing integrators (we recommend using velocity verlet).
- Adjusting mass of particles
- Adjusting particle number.
- Adjusting the mass of the center of mass (COM)

If the file is executed as `main`, the simulation will be run with following options:
- Running the simulation.
- Adjusting particle number.
- Adjusting time steps.
- Adjusting number of time steps.
- Adjusting number of snapshots.


Please make sure to read the code description/specification, since it will guide you through.
---------

**Important remarks:**

- To avoide infinity gravitational forces at infinitesimally small distances between particles, a cutoff distance is applied. The pair-wise distances between never drop below it.

- The COM is provided with a mass. This can be adjusted in "ff_simulation_3D.py".

---------

We recommend using VS_code, Spyder, or Jupyter notebook editiors to run this simulation. Enjoy :)  