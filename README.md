## Repository structure

All folders are isolated projects. `cd` into the folder to run/build them.

- `jobs`: jobs contain utility functions
- `tasks`: a task can be composed of many jobs

## Getting started with development tools

[poetry](https://python-poetry.org/docs/) is used as package manager. Ensure you've it!

[docker](https://docs.docker.com/) is used in this project to build, share, and run container applications. Ensure you've it!

#### Do this once
1. Install [python](https://www.python.org/downloads/) (version 3.11.5)
2. Install [poetry](https://python-poetry.org/docs/#installation) (version 1.7.1)
3. Install [docker](https://docs.docker.com/engine/install/). If you want to run docker as non-root user then you need to add it to the docker group (see [documentation](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)).
4. From the root directory of this repo, run
 
        make init

#### Do this happily ever after
1. Export the root directory of this repo as a `PYTHONPATH`
2. Start a (new) shell and activate the virtual environment

        poetry shell
        
3. We are storing Docker images at GitLab's integrated container registry by running:

        docker login git.imp.fu-berlin.de:5000 -u <GITLAB_USERNAME> -p <TOKEN> 

*Some Project Access Tokens are available and will be provided to members.*

## Local development
### jobs and tasks

Jobs and tasks are run inside of docker containers. There is a docker build script (`Dockerfile`) and can be run in `dev` mode via `docker run`. Please be aware it can take a while to build the initial image. At root directory, to build the image run:

    bash build/build-image.sh

The image is built now and ready to be used! The tests in the Docker container built from the image can be sanity-checked via :

    docker run -it --rm git.imp.fu-berlin.de:5000/comp-sci-project/jobs-n-tasks:local pytest jobs/tests/test_a_job.py

or for testing a task:

    docker run -it --rm git.imp.fu-berlin.de:5000/comp-sci-project/jobs-n-tasks:local pytest tasks/tests/test_a_task.py

For live experience (meaning you enter the container), run:

    docker run -it --rm git.imp.fu-berlin.de:5000/comp-sci-project/jobs-n-tasks:local /bin/bash

If you change code in `src`, you need to rebuild the image with `bash build/build-image.sh`. The `src` folder can also be mounted in the image, but the assumption is that life is already difficult as it is...

### interactive web-based simulation
In the `public` directory
- Run `python -m http.server`
- Go to `http://localhost:8000/public`

## Plug and simulate
*Requirements: python (version 3.11.5) and  poetry (version 1.7.1)*

All commands are to be carried out at `root` directory. The root directory of this repo needs to be added to `PYTHONPATH`.


0. Create virtualenv with necessary dependencies (do once)

        cd build/ && poetry install --no-root && cd ../

1. Activate virtualenv with necessary dependencies 

        cd build/ && poetry shell && cd ../

2. Direct simulation of our solar system

        python tasks/src/direct_simulation.py

3. Direct simulation of our solar system with noise in initial data

        python tasks/src/stability_analysis_solar_system.py

4. Run integration test of trajectories obtained through direct simulation against JPL data

        python tasks/tests/test_direct_simulation.py

5. Force field simulation of galaxy collision

        python tasks/src/ff_simulation_3D.py

*Free and open-source software for all souls! Technical support is, unfortunately, only for group members.*