import csv
import unittest

import matplotlib.pyplot as plt
import numpy as np

from jobs.src.integrator import verlet
from jobs.src.jpl_data_query import JPLoader
from jobs.src.system import GravitationalSystem
from tasks.src.utils import plot_orbits

import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

INPUT_PATH = "tasks/inputs/"
input_file = csv.DictReader(open(INPUT_PATH + "initial.csv", encoding="utf8"))


def E_tot(p: np.ndarray, q: np.ndarray, body_GMs: list) -> np.ndarray:
    n_time_steps, _ = p.shape
    n_bodies = len(body_GMs)
    p_arr = p.reshape(n_time_steps, n_bodies, 3)

    body_GMs_arr = np.array(body_GMs)

    E_kin = np.sum((1 / 2) * body_GMs_arr * np.linalg.norm(p_arr, axis=2), axis=1)
    E_pot = 0
    q_arr = q.reshape(n_time_steps, n_bodies, 3)

    for i in range(n_bodies):
        q_pos_i = q_arr[:, i, :].reshape(n_time_steps, 1, 3)
        q_pos_i = np.repeat(q_pos_i, 10, axis=1)
        j = [a for a in range(n_bodies) if a != i]
        E_pot -= np.sum(np.repeat(body_GMs_arr[i], 10) * body_GMs_arr[j] /
                        (np.linalg.norm(q_pos_i - q_arr[:, j, :], axis=2)),
                        axis=1)

    return E_kin + E_pot


def lin_mom_tot(p: np.ndarray, body_GMs: list) -> np.ndarray:

    n_time_steps, _ = p.shape
    n_bodies = len(body_GMs)
    body_GMs_arr = np.array(body_GMs)
    p_arr = p.reshape(n_time_steps, n_bodies, 3)

    return np.sum(body_GMs_arr * np.linalg.norm(p_arr, axis=2), axis=1)


def am_tot(p: np.ndarray, q: np.ndarray, body_GMs: list) -> np.ndarray:
    n_time_steps, _ = p.shape
    n_bodies = len(body_GMs)

    p_arr = p.reshape(n_time_steps, n_bodies, 3)
    q_arr = q.reshape(n_time_steps, n_bodies, 3)

    L_tot = np.zeros((n_time_steps, 3))

    for i in range(n_bodies):
        pos_i = q_arr[:, i, :].reshape(n_time_steps, 1, 3)
        velo_i = p_arr[:, i, :].reshape(n_time_steps, 1, 3)
        L_tot += body_GMs[i] * np.cross(pos_i, velo_i, axisa=2, axisb=2).reshape(n_time_steps, 3)

    return np.linalg.norm(L_tot, axis=1)


class IntegratorJPLTest(unittest.TestCase):

    def test_integrator_JPL(self):
        """
        Test functionalities of velocity-Verlet algorithm 
        using JPL data for solar system
        """

        id_list = [
            "Sun",
            "199",
            "299",
            "Geocenter",
            "301",
            "499",
            "599",
            "699",
            "799",
            "899",
            "999",
        ]

        bodies_list = [
            "Sun",
            "Mercury",
            "Venus",
            "Earth",
            "Moon",
            "Mars",
            "Jupiter",
            "Saturn",
            "Uranus",
            "Neptune",
            "Pluto",
        ]

        jpl_data = JPLoader(id_list)
        fig = plt.figure(figsize=(50.0, 50.0))
        ax = fig.add_subplot(autoscale_on=True, projection='3d')
        ax.set_xlim3d(-20, 20)  #7
        ax.set_ylim3d(-20, 20)
        ax.set_zlim3d(-20, 20)
        ax.grid()

        jpl_data.plot_orbits(ax, bodies_list)
        plt.show()

        trajs = []

        for vectors in jpl_data.data_list:
            trajs.append(np.array([vectors["x"], vectors["y"], vectors["z"]]))

        trajs = np.array(trajs)
        n_bodies, dim, n_time_steps = trajs.shape
        trajs = trajs.reshape(n_time_steps, n_bodies, dim)

        r0 = []
        v0 = []
        body_GMs = []
        body_names = []
        for data in input_file:
            r0.append(np.array([float(x) for x in [data['x'], data['y'], data['z']]]))
            v0.append(np.array([float(x) for x in [data['vx'], data['vy'], data['vz']]]))
            body_GMs.append(float(data['GM']))
            body_names.append(data['name'])

        def force(q: np.ndarray, body_GMs: list = body_GMs, dim: int = 3) -> np.ndarray:
            total_force = []
            for i in range(len(body_GMs)):
                force_i = 0
                for j in range(len(body_GMs)):
                    if j != i:
                        force_i -= body_GMs[j] * (q[dim * i:dim *
                                                    (i + 1)] - q[dim * j:dim *
                                                                 (j + 1)]) / np.linalg.norm(q[dim * i:dim *
                                                                                              (i + 1)] - q[dim * j:dim *
                                                                                                           (j + 1)])**3

                total_force.append(force_i)
            return np.array(total_force).flatten()

        r0 = np.array(r0).flatten()
        v0 = np.array(v0).flatten()

        solar_system = GravitationalSystem(
            r0=r0,
            v0=v0,
            m=np.ones(33),  #
            t=np.linspace(0, 10960, 1096),
            force=force,
            solver=verlet)

        t, p, q = solar_system.simulation()
        plot_orbits(q, body_names)
        q_comp = q.reshape(len(t), 11, 3)

        logger.info("Checking overlap of orbits... ")
        self.assertTrue(np.max(np.linalg.norm(q_comp, axis=2) - np.linalg.norm(trajs, axis=2)) < 35)

        logger.info("Checking total energy conservation... ")
        E_tot_sys = E_tot(p, q, body_GMs)
        self.assertTrue(np.greater(1e-8 + np.zeros(E_tot_sys.shape[0]), E_tot_sys - E_tot_sys[0]).all())
        plt.figure()
        plt.plot(t, E_tot_sys - E_tot_sys[0])
        plt.xlabel("$t$ (days)")
        plt.ylabel(r"$E-E_0 [\frac{G \dot M^2}{L}]$")
        plt.grid()
        plt.tight_layout()
        plt.show()

        logger.info("Checking total linear momentum conservation... ")
        lin_mom_sys = lin_mom_tot(p, body_GMs)
        self.assertTrue(np.greater(1e-8 + np.zeros(lin_mom_sys.shape[0]), lin_mom_sys - lin_mom_sys[0]).all())
        plt.figure()
        plt.plot(t, lin_mom_sys - lin_mom_sys[0])
        plt.xlabel("$t$ (days)")
        plt.ylabel(r"$|P-P_0| [\frac{ML}{T}]$")
        plt.grid()
        plt.tight_layout()
        plt.show()

        logger.info("Checking total angular momentum conservation... ")
        am_mom_sys = am_tot(p, q, body_GMs)
        self.assertTrue(np.greater(1e-20 + np.zeros(am_mom_sys.shape[0]), am_mom_sys - am_mom_sys[0]).all())
        plt.figure()
        plt.plot(t, am_mom_sys - am_mom_sys[0])
        plt.xlabel("$t$ (days)")
        plt.ylabel(r"$|L-L_0| [\frac{ML^2}{T}]$")
        plt.grid()
        plt.tight_layout()
        plt.show()


if __name__ == '__main__':
    unittest.main()
