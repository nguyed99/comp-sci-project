"""
This module performs stability analysis of the restricted 
3-body system using synthetic initial data
"""
import matplotlib.pyplot as plt
import numpy as np

from jobs.src.integrator import verlet
from jobs.src.system import GravitationalSystem
from tasks.src.utils import Scaler

# system settings
R_SE = 1  # distance between Earth and the Sun, 1 astronomical unit (AU)
R_L = 2.57e-3 * R_SE  # distance between Earth and the Moon

M_S = 1  # solar mass
M_E = 3.00e-6 * M_S
M_L = 3.69e-8 * M_S

T_E = 1  # earth yr
T_L = 27.3 / 365.3 * T_E

G = 4 * np.pi**2  # AU^3 / m / yr^2

# simulation settings
T = 0.3  # yr
n_order = 6
dt = 4**(-n_order)  # yr

x0 = np.array([
    R_SE,
    0,
    R_SE + R_L,
    0,
    0,
    R_SE * 2 * np.pi / T_E,
    0,
    R_SE * 2 * np.pi / T_E + 1 * R_L * 2 * np.pi / T_L,
])
SNR = [0.1, 0.05, 0.005]  # signal to noise ratio


# force computation
def force(q: np.ndarray) -> np.ndarray:
    r1 = q[0:2]
    r2 = q[2:4]

    return np.array([
        -G * M_E * (M_S * r1 / np.linalg.norm(r1)**3 + M_L * (r1 - r2) / np.linalg.norm(r1 - r2)**3),
        -G * M_L * (M_S * r2 / np.linalg.norm(r2)**3 + M_E * (r2 - r1) / np.linalg.norm(r2 - r1)**3),
    ]).flatten()


system = GravitationalSystem(r0=x0[:4],
                             v0=x0[4:],
                             m=np.array([M_E, M_E, M_L, M_L]),
                             t=np.linspace(0, T, int(T // dt)),
                             force=force,
                             solver=verlet)

t, p_org, q_org = system.simulation()

## Case 1: Gaussian noise added to position
scaler_1_0 = Scaler(x0[:4], new_max=1.0, new_min=0.0)
pos0_scaled = scaler_1_0.scale(x0[:4])

for j in SNR:
    q_pos_perturbed = []
    for i in range(100):
        pos0_perturbed = pos0_scaled + j * np.random.normal(size=4)
        pos0_perturbed = scaler_1_0.inverse_scale(pos0_perturbed)
        system = GravitationalSystem(r0=pos0_perturbed,
                                     v0=x0[4:],
                                     m=np.array([M_E, M_E, M_L, M_L]),
                                     t=np.linspace(0, T, int(T // dt)),
                                     force=force,
                                     solver=verlet)
        t, p, q = system.simulation()
        q_pos_perturbed.append(q)

    q_pos_perturbed = np.average(np.array(q_pos_perturbed), axis=0)

    plt.figure()
    plt.plot(q_org[:, 0], q_org[:, 1], label="Earth")
    plt.plot(0, 0, 'o', label="Sun", color='pink', markersize=10)
    plt.plot(q_pos_perturbed[:, 0], q_pos_perturbed[:, 1], label="av. perturbed Earth")
    plt.xlabel("x/AU")
    plt.ylabel("y/AU")
    plt.grid()
    plt.legend()
    plt.title("Perturbed initial positions")
    plt.tight_layout()

    plt.savefig(f"pert_init_pos_earth_SNR_{j}.png")

    plt.figure()
    plt.plot(q_org[:, 2] - q_org[:, 0], q_org[:, 3] - q_org[:, 1], label="Moon")
    plt.plot(0, 0, 'o', label="Sun", color='pink', markersize=10)
    plt.plot(q_pos_perturbed[:, 2] - q_pos_perturbed[:, 0],
             q_pos_perturbed[:, 3] - q_pos_perturbed[:, 1],
             label="av. perturbed Moon")
    plt.xlabel("x/AU")
    plt.ylabel("y/AU")
    plt.grid()
    plt.legend()
    plt.title("Perturbed initial positions")
    plt.tight_layout()
    plt.savefig(f"pert_init_pos_moon_SNR_{j}.png")

## Case 2: Gaussian noise added to velocity
scaler_1_0 = Scaler(x0[4:], new_max=1.0, new_min=0.0)
velo0_scaled = scaler_1_0.scale(x0[4:])

for j in SNR:
    q_velo_perturbed = []
    for i in range(100):
        velo0_perturbed = velo0_scaled + j * np.random.normal(size=4)
        velo0_perturbed = scaler_1_0.inverse_scale(velo0_perturbed)
        system = GravitationalSystem(r0=x0[:4],
                                     v0=velo0_perturbed,
                                     m=np.array([M_E, M_E, M_L, M_L]),
                                     t=np.linspace(0, T, int(T // dt)),
                                     force=force,
                                     solver=verlet)
        t, p, q = system.simulation()
        q_velo_perturbed.append(q)

    q_velo_perturbed = np.average(np.array(q_velo_perturbed), axis=0)

    plt.figure()
    plt.plot(q_org[:, 0], q_org[:, 1], label="Earth")
    plt.plot(0, 0, 'o', label="Sun", color='pink', markersize=10)
    plt.plot(q_velo_perturbed[:, 0], q_velo_perturbed[:, 1], label="av. perturbed Earth")
    plt.xlabel("x/AU")
    plt.ylabel("y/AU")
    plt.grid()
    plt.legend()
    plt.title("Perturbed initial velocities")
    plt.tight_layout()
    plt.savefig(f"pert_init_velo_earth_SNR_{j}.png")

    plt.figure()
    plt.plot(q_org[:, 2] - q_org[:, 0], q_org[:, 3] - q_org[:, 1], label="Moon")
    plt.plot(0, 0, 'o', label="Sun", color='pink', markersize=10)
    plt.plot(q_velo_perturbed[:, 2] - q_velo_perturbed[:, 0],
             q_velo_perturbed[:, 3] - q_velo_perturbed[:, 1],
             label="av. perturbed Moon")
    plt.xlabel("x/AU")
    plt.ylabel("y/AU")
    plt.grid()
    plt.legend()
    plt.title("Perturbed initial velocities")
    plt.tight_layout()

    plt.savefig(f"pert_init_velo_moon_SNR_{j}.png")

## Case 3: Gaussian noise added to position + velocity
veloscaler_1_0 = Scaler(x0[4:], new_max=1.0, new_min=0.0)
velo0_scaled = veloscaler_1_0.scale(x0[4:])
posscaler_1_0 = Scaler(x0[:4], new_max=1.0, new_min=0.0)
pos0_scaled = posscaler_1_0.scale(x0[:4])

for j in SNR:
    q_perturbed = []
    for i in range(100):
        pos0_perturbed = pos0_scaled + j * np.random.normal(size=4)
        pos0_perturbed = posscaler_1_0.inverse_scale(pos0_perturbed)
        velo0_perturbed = velo0_scaled + j * np.random.normal(size=4)
        velo0_perturbed = veloscaler_1_0.inverse_scale(velo0_perturbed)
        system = GravitationalSystem(r0=pos0_perturbed,
                                     v0=velo0_perturbed,
                                     m=np.array([M_E, M_E, M_L, M_L]),
                                     t=np.linspace(0, T, int(T // dt)),
                                     force=force,
                                     solver=verlet)
        t, p, q = system.simulation()
        q_perturbed.append(q)

    q_perturbed = np.average(np.array(q_perturbed), axis=0)

    plt.figure()
    plt.plot(q_org[:, 0], q_org[:, 1], label="Earth")
    plt.plot(0, 0, 'o', label="Sun", color='pink', markersize=10)
    plt.plot(q_perturbed[:, 0], q_perturbed[:, 1], label="av. perturbed Earth")
    plt.xlabel("x/AU")
    plt.ylabel("y/AU")
    plt.grid()
    plt.legend()
    plt.title("Perturbed initial position + velocities")
    plt.tight_layout()
    plt.savefig(f"pert_init_pos_velo_earth_SNR_{j}.png")

    plt.figure()
    plt.plot(q_org[:, 2] - q_org[:, 0], q_org[:, 3] - q_org[:, 1], label="Moon")
    plt.plot(0, 0, 'o', label="Sun", color='pink', markersize=10)
    plt.plot(q_perturbed[:, 2] - q_perturbed[:, 0], q_perturbed[:, 3] - q_perturbed[:, 1], label="av. perturbed Moon")
    plt.xlabel("x/AU")
    plt.ylabel("y/AU")
    plt.grid()
    plt.legend()
    plt.title("Perturbed initial position + velocities")
    plt.tight_layout()
    plt.savefig(f"pert_init_pos_velo_moon_SNR_{j}.png")
