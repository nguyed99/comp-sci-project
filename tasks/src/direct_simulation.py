"""
This module performs a direct gravitational simulation
for the solar system
"""

import csv

import numpy as np

from jobs.src.system import GravitationalSystem
from jobs.src.integrator import verlet
from tasks.src.utils import plot_orbits

INPUT_PATH = "tasks/inputs/"
input_file = csv.DictReader(open(INPUT_PATH + "initial.csv", encoding="utf8"))

r0 = []
v0 = []
body_GMs = []
body_names = []
for data in input_file:
    r0.append(np.array([float(x) for x in [data['x'], data['y'], data['z']]]))
    v0.append(np.array([float(x) for x in [data['vx'], data['vy'], data['vz']]]))
    body_GMs.append(float(data['GM']))
    body_names.append(data['name'])

r0 = np.array(r0).flatten()
v0 = np.array(v0).flatten()


def force(q: np.ndarray, body_GMs: list = body_GMs, dim: int = 3) -> np.ndarray:
    total_force = []
    for i in range(len(body_GMs)):
        force_i = 0
        for j in range(len(body_GMs)):
            if j != i:
                force_i -= body_GMs[j] * (q[dim * i:dim *
                                            (i + 1)] - q[dim * j:dim *
                                                         (j + 1)]) / np.linalg.norm(q[dim * i:dim *
                                                                                      (i + 1)] - q[dim * j:dim *
                                                                                                   (j + 1)])**3

        total_force.append(force_i)
    return np.array(total_force).flatten()


solar_system = GravitationalSystem(
    r0=r0,
    v0=v0,
    m=np.ones(33),  # gravitational masses
    t=np.linspace(0, 10960, 1096),
    force=force,
    solver=verlet)

t, p, q = solar_system.simulation()
plot_orbits(q, body_names)
