"""
This module performs stability analysis of the solar system using 
realistic initial data
"""

import csv

import numpy as np

from jobs.src.integrator import verlet
from jobs.src.system import GravitationalSystem
from tasks.src.utils import (plot_orbits, Scaler)

INPUT_PATH = "tasks/inputs/"
input_file = csv.DictReader(open(INPUT_PATH + "initial.csv", encoding="utf8"))

r0 = []
v0 = []
body_GMs = []
body_names = []
for data in input_file:
    r0.append(np.array([float(x) for x in [data['x'], data['y'], data['z']]]))
    v0.append(np.array([float(x) for x in [data['vx'], data['vy'], data['vz']]]))
    body_GMs.append(float(data['GM']))
    body_names.append(data['name'])

r0 = np.array(r0).flatten()
v0 = np.array(v0).flatten()

SNR = [0.005]  # signal to noise ratio


def force(q: np.ndarray, body_GMs: list = body_GMs, dim: int = 3) -> np.ndarray:
    total_force = []
    for i in range(len(body_GMs)):
        force_i = 0
        for j in range(len(body_GMs)):
            if j != i:
                force_i -= body_GMs[j] * (q[dim * i:dim *
                                            (i + 1)] - q[dim * j:dim *
                                                         (j + 1)]) / np.linalg.norm(q[dim * i:dim *
                                                                                      (i + 1)] - q[dim * j:dim *
                                                                                                   (j + 1)])**3

        total_force.append(force_i)
    return np.array(total_force).flatten()


solar_system = GravitationalSystem(
    r0=r0,
    v0=v0,
    m=np.ones(33),  #
    t=np.linspace(0, 10960, 1096),
    force=force,
    solver=verlet)

t, p_org, q_org = solar_system.simulation()

## Case 1: Gaussian noise added to position
# scaler_1_0 = Scaler(r0, new_max=1.0, new_min=0.0)
# pos0_scaled = scaler_1_0.scale(r0)

# for j in SNR:
#     q_pos_perturbed = []
#     for i in range(10):
#         pos0_perturbed = pos0_scaled + j * np.random.normal(size=33)
#         pos0_perturbed = scaler_1_0.inverse_scale(pos0_perturbed)
#         system = GravitationalSystem(r0=pos0_perturbed,
#                                      v0=v0,
#                                      m=np.ones(33),
#                                      t=np.linspace(0, 10960, 1096),
#                                      force=force,
#                                      solver=verlet)
#         t, p, q = system.simulation()
#         q_pos_perturbed.append(q)

#     q_pos_perturbed = np.average(np.array(q_pos_perturbed), axis=0)
#     plot_orbits(q_pos_perturbed, body_names=body_names)

## Case 2: Gaussian noise added to velocity
scaler_1_0 = Scaler(v0, new_max=1.0, new_min=0.0)
velo0_scaled = scaler_1_0.scale(v0)

for j in SNR:
    q_velo_perturbed = []
    for i in range(10):
        velo0_perturbed = velo0_scaled + j * np.random.normal(size=33)
        velo0_perturbed = scaler_1_0.inverse_scale(velo0_perturbed)
        system = GravitationalSystem(r0=r0,
                                     v0=velo0_perturbed,
                                     m=np.ones(33),
                                     t=np.linspace(0, 10960, 1096),
                                     force=force,
                                     solver=verlet)
        t, p, q = system.simulation()
        q_velo_perturbed.append(q)

    q_velo_perturbed = np.average(np.array(q_velo_perturbed), axis=0)
    plot_orbits(q_velo_perturbed, body_names=body_names)

## Case 3: Gaussian noise added to position + velocity
veloscaler_1_0 = Scaler(v0, new_max=1.0, new_min=0.0)
velo0_scaled = veloscaler_1_0.scale(v0)
posscaler_1_0 = Scaler(r0, new_max=1.0, new_min=0.0)
pos0_scaled = posscaler_1_0.scale(r0)

for j in SNR:
    q_perturbed = []
    for i in range(10):
        pos0_perturbed = pos0_scaled + j * np.random.normal(size=33)
        pos0_perturbed = posscaler_1_0.inverse_scale(pos0_perturbed)
        velo0_perturbed = velo0_scaled + j * np.random.normal(size=33)
        velo0_perturbed = veloscaler_1_0.inverse_scale(velo0_perturbed)
        system = GravitationalSystem(r0=pos0_perturbed,
                                     v0=velo0_perturbed,
                                     m=np.ones(33),
                                     t=np.linspace(0, 10960, 1096),
                                     force=force,
                                     solver=verlet)
        t, p, q = system.simulation()
        q_perturbed.append(q)

    q_perturbed = np.average(np.array(q_perturbed), axis=0)
    plot_orbits(q_perturbed, body_names=body_names)
