import matplotlib.pyplot as plt
import numpy as np


class Scaler(object):
    """
    Scaler class for min-max scaling
    """

    def __init__(self, data, new_max, new_min):
        self.own_min, self.own_max = np.min(data), np.max(data)
        self.new_max, self.new_min = new_max, new_min

    def scale(self, data: np.ndarray) -> np.ndarray:
        return (data - self.own_min) / (self.own_max - self.own_min) * (self.new_max - self.new_min) + self.new_min

    def inverse_scale(self, data: np.ndarray) -> np.ndarray:
        return (data - self.new_min) * (self.own_max - self.own_min) / (self.new_max - self.new_min) + self.own_min


def plot_orbits(q: np.ndarray, body_names: list):

    fig = plt.figure(figsize=(50.0, 50.0))
    ax = fig.add_subplot(autoscale_on=True, projection='3d')
    # ax.set_xlim3d(-7,7)
    # ax.set_ylim3d(-7,7)
    # ax.set_zlim3d(-7,7)

    ax.set_xlim3d(-20, 20)  #7
    ax.set_ylim3d(-20, 20)
    ax.set_zlim3d(-20, 20)

    ax.grid()

    for i, _ in enumerate(body_names):
        xs = q[:, 3 * i]
        ys = q[:, 3 * i + 1]
        zs = q[:, 3 * i + 2]

        ax.plot(xs, ys, zs, lw=1.5, label=body_names[i])
        ax.scatter(xs[-1], ys[-1], zs[-1])

    ax.legend()
    plt.show()
