"""
This module contains 1 integrator, which is at least 
of second consistency order and approrimately preserves 
the energy over long times.
"""

from collections.abc import Callable

import numpy as np


def verlet(F: Callable, q0: np.ndarray, p0: np.ndarray, m: np.ndarray, dt: float):
    """
    Velocity-Verlet integrator for one time step
    """
    p = p0
    q = q0

    p = p + 1 / 2 * F(q) * dt
    q = q + 1 / m * p * dt
    p = p + 1 / 2 * F(q) * dt

    return p, q
