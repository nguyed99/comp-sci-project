"""
This module contains functions to query and parse JPL Horizons 
on-line solar system data (see https://ssd-api.jpl.nasa.gov/doc/horizons.html).
"""

from astroquery.jplhorizons import Horizons


class JPLoader:

    def __init__(self, id_list):
        self.id_list = id_list

        self.data_list = []

        for jpl_id in id_list:

            body = Horizons(id=jpl_id,
                            location='@ssb',
                            epochs={
                                'start': '1970-1-1',
                                'stop': '2000-1-1',
                                'step': '10d'
                            },
                            id_type="majorbody")
            vecs = body.vectors()
            self.data_list.append(vecs)
            print("\t".join([str(x) for x in list(vecs[0]["targetname", "x", "y", "z", "vx", "vy", "vz"])]))

    def plot_orbits(self, plot, bodies_list):
        for i, vectors in enumerate(self.data_list):
            xs = vectors["x"]
            ys = vectors["y"]
            zs = vectors["z"]

            plot.plot(xs, ys, zs, lw=1.5, label=bodies_list[i])
            plot.legend()
            plot.scatter(xs[-1], ys[-1], zs[-1])
