"""
This unittest tests data query from JPL Horizons database 
"""

import unittest

import matplotlib.pyplot as plt

import astropy.units as u
from sunpy.coordinates import (get_body_heliographic_stonyhurst, get_horizons_coord)
from sunpy.time import parse_time


class IntegratorTest(unittest.TestCase):

    def test_data_query(self):
        perihelion_14 = parse_time('2022-12-11 13:16')
        psp = get_horizons_coord('Parker Solar Probe', {
            'start': perihelion_14 - 50 * u.day,
            'stop': perihelion_14 + 50 * u.day,
            'step': '180m'
        })
        earth = get_body_heliographic_stonyhurst('Earth', perihelion_14)

        def coord_to_polar(coord):
            return coord.lon.to_value('rad'), coord.radius.to_value('AU')

        fig = plt.figure()
        ax = fig.add_subplot(projection='polar')
        ax.plot(0, 0, 'o', label='Sun', color='orange')
        ax.plot(*coord_to_polar(earth), 'o', label='Earth', color='blue')
        ax.plot(*coord_to_polar(psp), label='PSP (as seen from Earth)', color='purple')
        ax.plot(*coord_to_polar(psp.transform_to(earth)),
                label='PSP (non-rotating frame)',
                color='purple',
                linestyle='dashed')
        ax.set_title('Stonyhurst heliographic coordinates')
        ax.legend(loc='upper center')

        plt.show()
