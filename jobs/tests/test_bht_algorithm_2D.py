"""
This unittest tests implementation of the BHT algorithm
for the restricted three-body (sun-earth-moon) problem.
The sun is fixed at the origin (center of mass). For 
simplicity, the moon is assumed to be in the eliptic plane,
so that vectors can be treated as two-dimensional. 
"""

import logging
import unittest

import matplotlib.pyplot as plt
import numpy as np

from jobs.src.integrator import verlet
from jobs.src.system import GravitationalSystem
from jobs.src.bht_algorithm_2D import MainApp, Particle

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

# system settings
R_SE = 1  # distance between Earth and the Sun, 1 astronomical unit (AU)
R_L = 2.57e-3 * R_SE  # distance between Earth and the Moon

M_S = 1  # solar mass
M_E = 3.00e-6 * M_S
M_L = 3.69e-8 * M_S

T_E = 1  # earth yr
T_L = 27.3 / 365.3 * T_E

G = 4 * np.pi**2  # AU^3 / m / yr^2

# simulation settings
T = 0.3  # yr
n_order = 6
dt = 4**(-n_order)  # yr


# force computation
def force(q: np.ndarray) -> np.ndarray:
    r1 = q[0:2]  # Earth coordinates
    r2 = q[2:4]  # Moon coordinates

    sun = Particle(0, 0, M_S)
    earth = Particle(r1[0], r1[1], M_E)
    moon = Particle(r2[0], r2[1], M_L)

    particles = [sun, earth, moon]

    barnes_hut = MainApp()  # Initialize Barnes-Hut algorithm instance
    barnes_hut.BuildTree(particles)  # Build the Barnes-Hut tree with particles
    barnes_hut.rootNode.ComputeMassDistribution()  #Compute the center of mass of the tree nodes

    f_earth = barnes_hut.rootNode.CalculateForceFromTree(earth)
    f_moon = barnes_hut.rootNode.CalculateForceFromTree(moon)

    return np.concatenate((f_earth, f_moon), axis=0)


class IntegratorTest(unittest.TestCase):

    def test_bht(self):
        """
        Test functionalities of velocity-Verlet algorithm
        """
        # vector of r0 and v0
        x0 = np.array([
            R_SE,
            0,
            R_SE + R_L,
            0,
            0,
            R_SE * 2 * np.pi / T_E,
            0,
            R_SE * 2 * np.pi / T_E + 1 * R_L * 2 * np.pi / T_L,
        ])

        system = GravitationalSystem(r0=x0[:4],
                                     v0=x0[4:],
                                     m=np.array([M_E, M_E, M_L, M_L]),
                                     t=np.linspace(0, T, int(T // dt)),
                                     force=force,
                                     solver=verlet)

        t, p, q = system.simulation()

        ## plotting trajectories
        plt.figure()
        plt.plot(q[:, 0], q[:, 1], label="Earth")
        plt.xlabel("x/AU")
        plt.ylabel("y/AU")
        plt.legend()
        plt.show()

        plt.figure()
        plt.plot(q[:, 2] - q[:, 0], q[:, 3] - q[:, 1], label="Moon seen from Earth")
        plt.xlabel("x/AU")
        plt.ylabel("y/AU")
        plt.legend()
        plt.show()

        ## checking total energy conservation
        H = np.linalg.norm(p[:,:2], axis=1)**2 / (2 * M_E) + np.linalg.norm(p[:,2:], axis=1)**2 / (2 * M_L) + \
            -G * M_S * M_E / np.linalg.norm(q[:,:2], axis=1) - G * M_S * M_L / np.linalg.norm(q[:,2:], axis=1) + \
            -G * M_E * M_L / np.linalg.norm(q[:,2:] - q[:,:2], axis=1)

        self.assertTrue(np.greater(1e-7 + np.zeros(H.shape[0]), H - H[0]).all())

        ## checking total linear momentum conservation
        P = p[:, :2] + p[:, 2:]
        self.assertTrue(np.greater(1e-10 + np.zeros(P[0].shape), P - P[0]).all())

        ## checking total angular momentum conservation
        L = np.cross(q[:, :2], p[:, :2]) + np.cross(q[:, 2:], p[:, 2:])
        self.assertTrue(np.greater(1e-8 + np.zeros(L.shape[0]), L - L[0]).all())

        ## checking error
        dts = [dt, 2 * dt, 4 * dt, 10 * dt, 50 * dt]
        errors_E = []
        errors_P = []
        ts = []
        for i in dts:
            system = GravitationalSystem(r0=x0[:4],
                                         v0=x0[4:],
                                         m=np.array([M_E, M_E, M_L, M_L]),
                                         t=np.linspace(0, T, int(T // i)),
                                         force=force,
                                         solver=verlet)

            t, p_t, q_t = system.simulation()


            H = np.linalg.norm(p_t[:,:2], axis=1)**2 / (2 * M_E) + np.linalg.norm(p_t[:,2:], axis=1)**2 / (2 * M_L) + \
            -G * M_S * M_E / np.linalg.norm(q_t[:,:2], axis=1) - G * M_S * M_L / np.linalg.norm(q_t[:,2:], axis=1) + \
            -G * M_E * M_L / np.linalg.norm(q_t[:,2:] - q_t[:,:2], axis=1)

            P = p_t[:, :2] + p_t[:, 2:]

            errors_E.append((H - H[0]) / i**2)
            errors_P.append(np.linalg.norm(P, axis=1) - np.linalg.norm(P[0]))
            ts.append(t)

        plt.figure()
        plt.plot(ts[0], errors_E[0], label="dt")
        plt.plot(ts[1], errors_E[1], linestyle='--', label="2*dt")
        plt.plot(ts[2], errors_E[2], linestyle=':', label="4*dt")
        plt.plot(ts[3], errors_E[3], linestyle='-.', label="10*dt")
        plt.plot(ts[4], errors_E[4], linestyle=':', label="50*dt")
        plt.xlabel("$t$")
        plt.ylabel("$\delta E(t)/(\Delta t)^2$")
        plt.legend()
        plt.title("Energy error")
        plt.tight_layout()
        plt.show()

        plt.figure()
        plt.plot(ts[0], errors_P[0], label="dt")
        plt.plot(ts[1], errors_P[1], linestyle='--', label="2*dt")
        plt.plot(ts[2], errors_P[2], linestyle=':', label="4*dt")
        plt.plot(ts[3], errors_P[3], linestyle='-.', label="10*dt")
        plt.plot(ts[4], errors_P[4], linestyle=':', label="50*dt")
        plt.xlabel("$t$")
        plt.ylabel("$P(t)-P(0)$")
        plt.legend()
        plt.title("Linear momentum error")
        plt.tight_layout()
        plt.show()

        ## checking time reversal: p -> -p
        x0 = np.concatenate((q[-1, :], -1 * p[-1, :] / np.array([M_E, M_E, M_L, M_L])), axis=0)
        system = GravitationalSystem(r0=x0[:4],
                                     v0=x0[4:],
                                     m=np.array([M_E, M_E, M_L, M_L]),
                                     t=np.linspace(0, T, int(T // dt)),
                                     force=force,
                                     solver=verlet)

        t, p_reverse, q_reverse = system.simulation()
        self.assertTrue(np.greater(1e-10 + np.zeros(4), q_reverse[-1] - q[0]).all())
        self.assertTrue(np.greater(1e-10 + np.zeros(4), p_reverse[-1] - p[0]).all())

        ## plotting trajectories
        plt.figure()
        plt.plot(q[:, 0], q[:, 1], label="Forward")
        plt.plot(q_reverse[:, 0], q_reverse[:, 1], '--', label="Backward")

        plt.xlabel("x/AU")
        plt.ylabel("y/AU")
        plt.title("Earth")
        plt.tight_layout()
        plt.legend()
        plt.show()

        plt.figure()
        plt.plot(q[:, 2] - q[:, 0], q[:, 3] - q[:, 1], label="Forward")
        plt.plot(q_reverse[:, 2] - q_reverse[:, 0], q_reverse[:, 3] - q_reverse[:, 1], '--', label="Backward")

        plt.xlabel("x/AU")
        plt.ylabel("y/AU")
        plt.title("Moon")
        plt.tight_layout()
        plt.legend()
        plt.show()


if __name__ == '__main__':
    unittest.main()
