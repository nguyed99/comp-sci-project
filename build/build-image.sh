#!/bin/bash
set -e

CWD=$(cd $(dirname $0); pwd)

REGISTRY=git.imp.fu-berlin.de:5000/comp-sci-project

IMAGE=${REGISTRY}/jobs-n-tasks:${RELEASE:-local}
BASE_VERSION="3.11.5-slim"

echo Building $IMAGE
docker build \
    --force-rm \
    --rm=true \
    -f "$CWD/Dockerfile" \
    --platform linux/amd64 \
    --build-arg BASE_VERSION=$BASE_VERSION \
    -t $IMAGE \
    "$CWD/.."